//! @file quadratic_equation.h
#ifndef QUAD_EQ
#define QUAD_EQ

/*! \brief A structure that contains two components for each root.
 * 
 * This structure stores a solution of the quadratic equation: ax^2 + bx + c = 0. 
 * Each root consists of two parts: real part and complex part. If discriminant 
 * that is counted for an equation is greater or equal to zero, the complex parts 
 * of each root will be set to zero. In all the other cases complex part will be 
 * computed as well as the real part.
 * 
 * @param r1 real part of the first root. It has a double type.
 * @param r1comp complex part of the first root. It has a double type.
 * @param r2 real part of the second root. It has a double type.
 * @param r2comp complex part of the second root. It has a double type.
 */
typedef struct equation_roots
{
    double r1;      /**< real part of the first root. It has a double type. */
    double r1comp;  /**< complex part of the first root. It has a double type. */
    double r2;      /**< real part of the second root. It has a double type. */
    double r2comp;  /**< complex part of the second root. It has a double type. */

} equation_roots_t;


/*! \brief Function that computes roots of the quadratic equation.
 * 
 * This function takes three double numbers that are the coefficients 
 * of the quadratic equation: ax^2 + bx + c = 0, and then computes the 
 * roots of the quadratic equation using quadratic formula. 
 * There are two ways to compute the result:
 *  - if the discriminant of the equation is greater than zero
 *    the roots of the equation are calculated as usual and this function returns
 *    a structure with zeros as complex parts of roots 
 *  - if the discriminant is less than zero this function will calculate every root 
 *    as a complex number, that consists of two parts: a real one and a complex one.
 * There are four possible ways of how will look the returning structure:
 *  - if the discriminant is greater or equal to zero, the returning structure wiil 
 *    have only two real fields filled;
 *  - if the discriminant is less than zero, than the structure will be returned 
 *    filled with four or two values, depending on the roots of the equation as they 
 *    could have both real and complex parts or only a complex part;
 *  - if the function got incorrect numbers as an input (e.g. zero as an "a" parameter),
 *    the structure will return filled with NAN (Not A Number) in all four fields.
 * 
 * @param a is the coefficient near x^2 in an equation ax^2 + bx + c = 0. 
 * It has a double type.
 * @param b is the coefficient near x in an equation ax^2 + bx + c = 0. 
 * It has a double type.
 * @param c is the coefficient near the free term in an equation ax^2 + bx + c = 0. 
 * It has a double type.
 * @returns a structure, that contains four fields, 
 * two for each root: a real part and a complex part. If the equation's coefficient "a" 
 * is less than 0, every field will contain NAN.
 * 
 */
equation_roots_t solve_equation (double , double , double );

#endif /* QUAD_EQ */
