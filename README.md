In this project you can see a system that solves quadratic equations: ax^2 + bx+ c = 0. The main function of the system is the "solve_equation" function. It takes three real numbers: a, b, c, and returns a structure, that has 4 fields, two for each root: a real part and a complex part of the root. 
This module also uses documenting system DoxyGen and has Unit-testing.

DoxyGen generates: 
 - a very convenient web page, that you can see if you open an index.html file in the doc/html folder;
 - an .rtf file that can be opened with Microsoft Word; it could be found in doc/rtf folder.
You can also find a PDF file in doc/rtf folder, but notice that it is not generated automatically! 

Unit-testing could be executed in two ways:
 - with the terminal command "make check";
 - automatically when you try and build the project ("make" command).

В данном проекте представлена система для решения квадратных уравнений: ax^2 + bx + c = 0. Основной функцией является функция solve_equation, которая принимает в качестве аргументов три действительный числа: a, b, c, и возвращает структуру, имеющую 4 поля, по два на каждый корень: действительная часть и комплексная часть. 
В данном модуле также используется система документирования DoxyGen и присутствует Unit-тестирование.

DoxyGen генерирует:
 - очень удобный сайт, на который можно попасть, если открыть файл index.html из папки doc/html в любом удобном браузере;
  - .rtf файл в папке /doc/rtf, который можно открыть с помощью Microsoft Word.
Также, в папке /doc/rtf есть PDF файл, но он не генерируется автоматически!

Unit-тестирование можно запустить двумя способами:
 - командой "make check" в терминале;
 - оно запустится автоматически при сборке проекта (команда "make").