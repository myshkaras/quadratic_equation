//! @file test.h
#ifndef TEST
#define TEST
#include "../include/quadratic_equation.h" 

/*! \brief Function that tests "solve_equation" function in case of correct coefficients
 * 
 * This function is made to test the "solve_equation" function in such cases 
 * that the given arguments are correct. It takes all three coefficients of the 
 * quadratic equation, the structure containing correct roots of the equation and 
 * a sequence number of the test. 
 * It calls the function and then compares the resulting roots with the ones given 
 * as the "correct" roots using assert() finction.
 * It also prints the sequence number of the test and an "OK" string (in case of successful testing) 
 * for the convenience, so the user knows in which test the values of the given roots and the 
 * "correct" ones where not equal
 * 
 * @param a is the coefficient near x^2 in an equation ax^2 + bx + c = 0. 
 * It has a double type.
 * @param b is the coefficient near x in an equation ax^2 + bx + c = 0. 
 * It has a double type.
 * @param c is the coefficient near the free term in an equation ax^2 + bx + c = 0. 
 * It has a double type.
 * @param correctRoots is a structure that contains "correct" roots of the given equation. 
 * It has an equation_roots_t type.
 * @param test_num is a sequence number of the test. It is printed to the terminal in the 
 * process of testing so that you could know which test caused termination of execution.
 */
void test_SE_func(double a, double b, double c, equation_roots_t correctRoots, int test_num);

/*! \brief Function that tests "solve_equation" function in case of incorrect coefficients
 * 
 * This function is made to test the "solve_equation" function in such cases 
 * that the given arguments are incorrect (e.g. "a" coefficient is equal to zero). 
 * It takes all three coefficients of the quadratic equation and a sequence number of the test. 
 * It calls the function and then checks if the fields of the returned structure are 
 * NAN using isnan() function inside of an assert() finction.
 * It also prints the sequence number of the test and an "OK" string (in case of successful testing) 
 * for the convenience, so the user knows in which test the values of the returning structure were
 * filled not with NAN, but with some other values.
 * 
 * @param a is the coefficient near x^2 in an equation ax^2 + bx + c = 0. 
 * It has a double type.
 * @param b is the coefficient near x in an equation ax^2 + bx + c = 0. 
 * It has a double type.
 * @param c is the coefficient near the free term in an equation ax^2 + bx + c = 0. 
 * It has a double type.
 * @param test_num is a sequence number of the test. It is printed to the terminal in the 
 * process of testing so that you could know which test caused termination of execution.
 */
void test_SE_func_nan(double a, double b, double c, int test_num);

/*! \brief Function that is made to do the testing
 * 
 * This function is made to do the testing using "test_SE_func" and "test_SE_func_nan" functions.
 * It is highly recommended to invoke all test functions inside this function instead of doing it 
 * inside of the main() function because main() function should be left as clean as posiible.
 */
void check(void);

#endif /* TEST */
