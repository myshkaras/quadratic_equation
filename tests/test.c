//! @file test.c
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include "../include/quadratic_equation.h" 
#include "test.h"


void test_SE_func(double a, double b, double c, equation_roots_t correctRoots, int test_num)
{
    printf ("-------- Test #%i --------\n", test_num);
    equation_roots_t test_roots = solve_equation(a, b, c);
    assert(test_roots.r1 == correctRoots.r1);
    assert(test_roots.r1comp == correctRoots.r1comp);
    assert(test_roots.r2 == correctRoots.r2);
    assert(test_roots.r2comp == correctRoots.r2comp);
    printf("OK\n");
}

void test_SE_func_nan(double a, double b, double c, int test_num)
{
    printf ("-------- Test #%i --------\n", test_num);
    equation_roots_t test_roots = solve_equation(a, b, c);
    assert(isnan(test_roots.r1) == 1);
    assert(isnan(test_roots.r1comp) == 1);
    assert(isnan(test_roots.r2) == 1);
    assert(isnan(test_roots.r2comp) == 1);
    printf("OK\n");
}

void check(void)
{
    int test_num = 1;
    /* Testing situations when function should return NAN in all structure fields */
    test_SE_func_nan(0.0, 0.0, 0.0, test_num); ++test_num;
    test_SE_func_nan(0.0, 0.0, 6.0, test_num); ++test_num;
    test_SE_func_nan(0.0, 0.0, -7.0, test_num); ++test_num;
    test_SE_func_nan(0.0, 2.0, 18.0, test_num); ++test_num;
    test_SE_func_nan(0.0, -2.0, 16.0, test_num); ++test_num;
    test_SE_func_nan(0.0, 2.0, -6.0, test_num); ++test_num;
    test_SE_func_nan(0.0, -2.0, -6.0, test_num); ++test_num;

    /* Testing situations when function should return only real roots */
    equation_roots_t cR1 = {.r1 = -1.0, .r1comp = 0.0, .r2 = -1.0, .r2comp = 0.0};
    test_SE_func(1.0, 2.0, 1.0, cR1, test_num); ++test_num;
    equation_roots_t cR2 = {.r1 = 1.0, .r1comp = 0.0, .r2 = 1.0, .r2comp = 0.0};
    test_SE_func(1.0, -2.0, 1.0, cR2, test_num); ++test_num;
    equation_roots_t cR3 = {.r1 = -1.0, .r1comp = 0.0, .r2 = 3.0, .r2comp = 0.0};
    test_SE_func(-1.0, 2.0, 3.0, cR3, test_num); ++test_num;
    equation_roots_t cR4 = {.r1 = 1.0, .r1comp = 0.0, .r2 = -3.0, .r2comp = 0.0};
    test_SE_func(1.0, 2.0, -3.0, cR4, test_num); ++test_num;
    equation_roots_t cR5 = {.r1 = 1.0, .r1comp = 0.0, .r2 = 1.0, .r2comp = 0.0};
    test_SE_func(-1.0, 2.0, -1.0, cR5, test_num); ++test_num;
    equation_roots_t cR6 = {.r1 = -3.0, .r1comp = 0.0, .r2 = 1.0, .r2comp = 0.0};
    test_SE_func(-1.0, -2.0, 3.0, cR6, test_num); ++test_num;
    equation_roots_t cR7 = {.r1 = 3.0, .r1comp = 0.0, .r2 = -1.0, .r2comp = 0.0};
    test_SE_func(1.0, -2.0, -3.0, cR7, test_num); ++test_num;
    equation_roots_t cR8 = {.r1 = -1.0, .r1comp = 0.0, .r2 = -1.0, .r2comp = 0.0};
    test_SE_func(-1.0, -2.0, -1.0, cR8, test_num); ++test_num;
    equation_roots_t cR9 = {.r1 = 2.0, .r1comp = 0.0, .r2 = 0.0, .r2comp = 0.0};
    test_SE_func(1.0, -2.0, 0.0, cR9, test_num); ++test_num;
    equation_roots_t cR10 = {.r1 = 0.0, .r1comp = 0.0, .r2 = 0.0, .r2comp = 0.0};
    test_SE_func(1.0, 0.0, 0.0, cR10, test_num); ++test_num;

    /* Testing situations when function should return complex roots */
    equation_roots_t cR11 = {.r1 = -0.5, .r1comp = 1.5, .r2 = -0.5, .r2comp = -1.5};
    test_SE_func(2.0, 2.0, 5.0, cR11, test_num); ++test_num;
    equation_roots_t cR12 = {.r1 = 0.5, .r1comp = 1.5, .r2 = 0.5, .r2comp = -1.5};
    test_SE_func(2.0, -2.0, 5.0, cR12, test_num); ++test_num;
    equation_roots_t cR13 = {.r1 = 0.0, .r1comp = 1.0, .r2 = 0.0, .r2comp = -1.0};
    test_SE_func(1.0, 0.0, 1.0, cR13, test_num); ++test_num;
}
