//! @file quadratic_equation.c

#include <math.h>
#include "../include/quadratic_equation.h"

#include <stdio.h>

equation_roots_t solve_equation (double a, double b, double c)
{
    equation_roots_t eqr = {0, 0, 0, 0};
    if (a == 0)
    {
        #ifdef NAN
        eqr.r1 = NAN;
        eqr.r1comp = NAN;
        eqr.r2 = NAN;
        eqr.r2comp = NAN;
        #endif /* NAN */

        return eqr;
    }

    double discriminant = (b * b) - 4.0 * a * c;

    if (discriminant < 0)
    {
        eqr.r1 = -b / (2 * a);
        eqr.r1comp = sqrt(-discriminant) / (2 * a); 
        eqr.r2 = -b / (2 * a);
        eqr.r2comp = - sqrt(-discriminant) / (2 * a);
    }
    else
    {
        eqr.r1 = (-b + sqrt(discriminant)) / (2 * a);
        eqr.r1comp = 0;
        eqr.r2 = (-b - sqrt(discriminant)) / (2 * a);
        eqr.r2comp = 0;
    }
    
    return eqr;
}
