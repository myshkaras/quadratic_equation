CC := CLang
CFLAGS := -pedantic -Wall -c
SRC := $(wildcard ./src/*.c)
SRC_T := $(wildcard ./tests/*.c)
OBJS := $(SRC:.c=.o)
OBJS_T := $(SRC_T:.c=)

.PHONY: all clean clean_lib check
all: clean_lib check lib_quadratic_equation.a lib_quadratic_equation.so clean

lib_quadratic_equation.a: $(OBJS)
	@ar rc $@ $^
	$(info Static library is built)

lib_quadratic_equation.so: $(OBJS)
	@$(CC) -shared -fPIC -o $@ $^
	$(info Dynamic library is built)

$(OBJS): $(SRC)
	@$(CC) $(CFLAGS) -o $@ $^
	$(info "quadratic_equation.o" is built)

test: $(SRC) $(SRC_T)
	@$(CC) -o ./tests/$@ $^
	$(info Built test file for check target)

check: test
	@./tests/test
	$(info Testing units)

clean: 
	@rm -f $(OBJS) $(OBJS_T)

clean_lib:
	@rm -f lib_quadratic_equation.a lib_quadratic_equation.so
